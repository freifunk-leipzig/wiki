# Freifunk Leipzig Wiki



## Kontakt

__E-Mail__

info@leipzig.freifunk.net

__Twitter__

[@freifunkleipzig](https://twitter.com/freifunk_l?lang=de)

__Gitlab__

[freifunk-leipzig](https://gitlab.com/freifunk-leipzig)

__Matrix-Chat)__

[Riot](https://app.element.io/#/room/#freifunk:chat.dezentrale.space)